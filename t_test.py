import pandas as pd
import scipy.stats as stats
import statsmodels.api as sm
import matplotlib.pyplot as plt
from foldchange import get_genes_of_interest
from parser import read_data


def get_significant_genes(data, smoker_ids, non_smoker_ids, threshold):
    p_values = pd.Series()
    for gene in data.index:
        t_statistic, p_value = stats.ttest_ind(data[smoker_ids].loc[gene], data[non_smoker_ids].loc[gene])
        if p_value <= threshold:
            p_values.loc[gene] = p_value
        p_values = p_values.sort_values()
    return p_values


if __name__ == '__main__':
    show_boxplots = False
    show_qq_plots = False
    save_to_file = False
    significance = 0.01
    variables_to_plot = 5

    data, log_data, smoker_ids, non_smoker_ids = read_data()
    genes_of_interest = get_genes_of_interest(data, smoker_ids, non_smoker_ids)[1]
    filtered_log_data = log_data.loc[genes_of_interest]

    p_values = get_significant_genes(filtered_log_data, smoker_ids, non_smoker_ids, threshold=significance)

    if save_to_file:
        p_values.to_csv("data/p_values.csv", sep="\t")
    else:
        print(p_values)

    if show_boxplots:
        # bei knappen 10 Messungen pro Merkmal macht eine Beurteilung der Verteilung mMn keinen großen Sinn
        # hier werden die ersten x Merkmale als Boxplot angezeigt
        filtered_log_data[smoker_ids].head(variables_to_plot).T.boxplot()
        if save_to_file:
            plt.savefig("plots/smoker_boxplots.png")
        else:
            plt.show()

        plt.clf()
        filtered_log_data[non_smoker_ids].head(variables_to_plot).T.boxplot()
        if save_to_file:
            plt.savefig("plots/non_smoker_boxplots.png")
        else:
            plt.show()
    if show_qq_plots:
        fig, axes = plt.subplots(1, variables_to_plot, figsize=(15, 4))
        for i in range(variables_to_plot):
            sm.qqplot(filtered_log_data[smoker_ids].iloc[i], line='s', ax=axes[i])
            axes[i].set_title(f"{filtered_log_data.index[i]}")
        plt.suptitle("Smokers")
        plt.tight_layout()
        if save_to_file:
            plt.savefig("plots/smoker_qq_plots.png")
        else:
            plt.show()

        fig, axes = plt.subplots(1, variables_to_plot, figsize=(15, 4))
        for i in range(variables_to_plot):
            sm.qqplot(filtered_log_data[non_smoker_ids].iloc[i], line='s', ax=axes[i])
            axes[i].set_title(f"{filtered_log_data.index[i]}")
        plt.suptitle("Non-Smokers")
        plt.tight_layout()
        if save_to_file:
            plt.savefig("plots/non_smoker_qq_plots.png")
        else:
            plt.show()
