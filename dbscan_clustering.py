from parser import read_data
from sklearn.cluster import DBSCAN
import numpy as np
from variance import get_highest_variance_genes
from normalize import normalize

if __name__ == '__main__':
    data, log_data, smoker_ids, non_smoker_ids = read_data()
    filtered_unnormalized_data = get_highest_variance_genes(data, dims=500).drop("Variance", axis=1)
    filtered_normalized_data = normalize(filtered_unnormalized_data)
    data = filtered_normalized_data

    # keine unterschiedlichen Cluster werden gefunden
    # Ich nehme an bei so vielen Dimensionen ist der Noise zu groß,
    # sodass sich stets Verbindungen zwischen den möglichen Clustern finden
    for eps in np.arange(0.05, 5, 0.05):
        for min_samples in range(2, 5):
            dbscan = DBSCAN(eps=eps, min_samples=min_samples)

            dbscan.fit(data.T.values)

            labels = dbscan.labels_

            # keine Cluster relevant die alle Daten verknüpfen
            if len(set(labels)) > 1:
                print(f"eps: {eps}\tmin_samples: {min_samples}")
                print("Cluster labels:", labels)

                cluster_samples = {}

                # Iterate through the samples and their labels
                for sample, label in zip(data.index, labels):
                    if label not in cluster_samples:
                        cluster_samples[label] = [sample]
                    else:
                        cluster_samples[label].append(sample)

                # Print samples in each cluster
                for cluster, samples in cluster_samples.items():
                    print(f"Cluster {cluster}:")
                    for sample in samples:
                        print(sample)
                    print()


