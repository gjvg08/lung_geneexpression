from parser import read_data
from variance import get_highest_variance_genes
from normalize import normalize
from foldchange import get_genes_of_interest
from scipy.cluster.hierarchy import dendrogram, linkage
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

if __name__ == '__main__':
    save_to_file = False
    data, log_data, smoker_ids, non_smoker_ids = read_data()

    filtered_unnormalized_data = get_highest_variance_genes(data, dims=500).drop("Variance", axis=1)
    filtered_normalized_data = normalize(filtered_unnormalized_data)

    _, genes_of_interest = get_genes_of_interest(data, smoker_ids, non_smoker_ids)
    foldchange_unnormalized_data = data.loc[genes_of_interest]
    foldchange_normalized_data = normalize(foldchange_unnormalized_data)

    data = foldchange_normalized_data

    data = data.transpose()

    linked = linkage(data, 'single')
    plt.figure(figsize=(10, 6))
    dendrogram(linked, labels=data.index, color_threshold=0, leaf_font_size=8,
               above_threshold_color='gray')
    plt.xlabel('Data Points')
    plt.ylabel('Distance')
    plt.title('HC normalized Data, foldchange filtered')

    ax = plt.gca()
    labels = ax.get_xmajorticklabels()
    for label in labels:
        sample_id = label.get_text()
        if sample_id in smoker_ids:
            label.set_color('red')
        else:
            label.set_color('green')

    legend_patches = [mpatches.Patch(color='red', label='Smokers'), mpatches.Patch(color='green', label='Non-Smokers')]
    plt.legend(handles=legend_patches)

    if save_to_file:
        plt.savefig("plots/HC_normalized_foldchange.png")
    else:
        plt.show()
