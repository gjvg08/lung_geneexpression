import pandas as pd
from parser import read_data
from sklearn.preprocessing import StandardScaler

def normalize(data):
    scaler = StandardScaler()
    normalized_data = scaler.fit_transform(data)
    normalized_dataframe = pd.DataFrame(normalized_data, columns=data.columns, index=data.index)
    return normalized_dataframe


if __name__ == '__main__':
    data, log_data, smoker_ids, non_smoker_ids = read_data()
    normalized_data = normalize(data)
    print(normalized_data)
