from parser import read_data


def get_highest_variance_genes(data, dims=500):
    # Returns variance as added column to the original data
    tmp = data.copy()
    variance = tmp.var(axis=1).round(2)
    tmp["Variance"] = variance
    tmp = tmp.sort_values(by="Variance", ascending=False)
    filtered_data = tmp[:dims]
    return filtered_data


if __name__ == '__main__':
    dims = 500
    save_to_file = False
    data, log_data, smoker_ids, non_smoker_ids = read_data()

    filtered_data = get_highest_variance_genes(data, dims)

    if save_to_file:
        filtered_data["Variance"].to_csv("data/variance.csv", sep="\t")
    else:
        print(filtered_data)

    # 100 mehr Gene mitgezählt, falls am unteren Ende unterschiedliche Gene in den besten dims Genen landen
    # normalized_gene_order = get_highest_variance_genes(data, dims + 100, True).index.tolist()
    # raw_gene_order = get_highest_variance_genes(data, dims + 100, False).index.tolist()
    #
    # diffs = [abs(raw_gene_order.index(el) - i) for i, el in enumerate(normalized_gene_order[:dims])]
    # mean_diff = sum(diffs) / len(diffs)
    # print("Verschiebung durch Normalisierung pro Gen", diffs)
    # print("Durschnittliche Verschiebung durch Normalisierung:", mean_diff)
