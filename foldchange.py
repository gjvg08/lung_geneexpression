import pandas as pd
from parser import read_data


def get_genes_of_interest(data, smoker_ids, non_smoker_ids, treshhold=2):
    smoker_means = data[smoker_ids].mean(axis=1)
    non_smoker_means = data[non_smoker_ids].mean(axis=1)

    means = pd.concat([smoker_means, non_smoker_means], axis=1, keys=["smoker", "non-smoker"])
    means["foldchange"] = means["smoker"] / means["non-smoker"]
    # foldchange > 2 and < 0.5
    filtered_means = means[(means["foldchange"] < 1 / treshhold) |
                           (means["foldchange"] > treshhold)].sort_values("foldchange", ascending=False)

    tmp = filtered_means.copy()
    tmp["foldchange"] = tmp["foldchange"].apply(lambda x: x if x > 1 else 1 / x)
    tmp = tmp.sort_values("foldchange", ascending=False)
    # returns a list of relevant genes with foldchange being small OR big
    genes_of_interest = tmp.index.tolist()
    return filtered_means, genes_of_interest


if __name__ == '__main__':
    save_to_file = False
    data, log_data, smoker_ids, non_smoker_ids = read_data()

    filtered_means, genes_of_interest = get_genes_of_interest(data, smoker_ids, non_smoker_ids)
    filtered_means = filtered_means.reindex(genes_of_interest)
    if save_to_file:
        filtered_means.to_csv("data/foldchange_reindexed.csv", sep="\t")
    else:
        print(filtered_means)
        print(genes_of_interest)
