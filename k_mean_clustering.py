from parser import read_data
from variance import get_highest_variance_genes
from normalize import normalize
import numpy as np
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.metrics import adjusted_rand_score

if __name__ == '__main__':
    data, log_data, smoker_ids, non_smoker_ids = read_data()
    highest_variance_data = get_highest_variance_genes(data, dims=500)
    data = highest_variance_data.drop('Variance', axis=1)
    data = normalize(data)


    num_clusters = 2
    kmeans = KMeans(n_clusters=num_clusters)

    kmeans.fit(data.T)
    cluster_labels = kmeans.labels_
    cluster_centers = kmeans.cluster_centers_

    true_labels = np.array([0 if id in non_smoker_ids else 1 for id in data.columns.tolist()])

    print("True labels:\t", true_labels)
    print("Cluster labels:\t", cluster_labels)

    for i in range(num_clusters):
        print(f"Cluster {i + 1}:")
        cluster_samples = data.T[cluster_labels == i]
        print("Number of samples:", len(cluster_samples))
        print("Samples:", data.columns[cluster_labels == i].tolist())
        print()

    ari_score = adjusted_rand_score(true_labels, cluster_labels)
    print("Adjusted Rand Index(ARI) score:", ari_score)

    silhouette_avg = silhouette_score(data.T, cluster_labels)
    print("Average silhouette score:", silhouette_avg)