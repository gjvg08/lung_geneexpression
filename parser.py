import pandas as pd
import numpy as np


def read_data(data_path="data/GSE4498_series_data.txt",
              metadata_path="data/metadata.txt"):
    data = pd.read_table(data_path, index_col=0, low_memory=False)
    metadata = pd.read_table(metadata_path, index_col=0, sep="\s", header=None)
    log_data = np.log(data)
    patient_ids = list(data.columns)
    smoker_ids = [id for id in patient_ids if metadata.loc[id][1] == "smoker"]
    non_smoker_ids = [id for id in patient_ids if metadata.loc[id][1] == "non-smoker"]
    return data, log_data, smoker_ids, non_smoker_ids


def read_gene_data(file_path="data/HG-U133-GPL570-39741.txt"):
    gene_id_dict = {}
    with open(file_path, 'r') as file:
        for line in file:
            split_line = line.split(',', 1)
            gene_id = split_line[0].strip('"')
            info = split_line[1].rstrip()
            gene_id_dict[gene_id] = info
    return gene_id_dict


def combine_id_csv_to_info(csv_file_path):
    gene_id_dict = read_gene_data()
    csv_data = pd.read_csv(csv_file_path, delimiter='\t', index_col=0)
    csv_data["Info"] = pd.Series(gene_id_dict)
    return csv_data


if __name__ == '__main__':
    save_to_file = False
    file_name = "foldchange_reindexed_plus_info"
    data_plus_info = combine_id_csv_to_info("data/foldchange_reindexed.csv")
    if save_to_file:
        data_plus_info.to_csv("data/" + file_name, sep="\t")
    else:
        print(data_plus_info)
